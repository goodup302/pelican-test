dpkg --configure -a
apt-get update -y
apt-get upgrade -y
apt autoremove -y


#Pelican
apt-get install -y python-virtualenv python-dev python-pip
apt-get install -y pelican markdown
pip install typogrify
#instal pelican theme
git clone --recursive https://github.com/nasskach/pelican-blueidea.git ~/pelican-themes/blueidea
pelican-themes --install ~/pelican-themes/blueidea --verbose
#build pelican
cd pelican-test
make clean && make html
#start server
#rm nohup.out
#nohup sudo make serve-global &


#Docker
apt-get remove -y docker docker-engine docker.io
apt install -y docker.io
systemctl start docker
systemctl enable docker


#Gitlab
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
apt-get install gitlab-ce -y
#nano /etc/gitlab/gitlab.rb
#external_url 'http://192.168.33.10'
gitlab-ctl reconfigure


#Gitlab Runners
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
dpkg -i gitlab-runner_amd64.deb
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
#gitlab-runner register --non-interactive --executor 'shell' --url 'http://127.0.0.1/' --registration-token 'G5oXYUhRt8fZxrvfwYPN'
#gitlab-runner register --non-interactive --executor docker --url 'http://127.0.0.1/' --registration-token 'G5oXYUhRt8fZxrvfwYPN' --docker-image ubuntu:18.04
#gitlab-runner start
#gitlab-runner verify