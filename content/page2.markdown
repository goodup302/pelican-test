title: Hello world
slug: hello-world
category: bands
date: 2017-06-09
modified: 2017-06-09

# hello-world

Bonjour, c’est tout pour moi est un film franco-belge sortie en 2017 et
réalisé par Nawell Madani et Ludovic Colbeau-Justin. La police utilisée
pour le titre du film sur l’affiche est Lemon/Milk. Lemon/Milk est une
famille de police sans empattements conçue par Marsnev. Cette famille de
police se décline en différentes graisses, soit maigre, normal et gras
avec le style italique. Vous pouvez la télécharger gratuitement ici. Si
vous ne voulez pas télécharger la police mentionnée ci-dessus, mais que
vous souhaitez simplement créer en ligne des logos ou images simples à
partir de texte, utilisez simplement le générateur ci-dessous. Celui-ci
convertira le texte que vous aurez saisi en images ou en logos, et vous
pouvez ensuite cliquer dessus pour télécharger un fichier, ou bien
cliquer sur le bouton « INTÉGRER » pour obtenir les liens permettant de
l'intégrer sur internet.